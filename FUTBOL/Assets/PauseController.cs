﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{

    public GameObject panel;
    public bool pausaActiva;
    public Button botonFocus;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {

            if (pausaActiva) {
                pausaOff();
                pausaActiva = false;
            }
            else
            {
                pausaOn();
                pausaActiva = true;
            }
        
        
        }
    }

    public void pausaOn() {
        Time.timeScale = 0;
        panel.SetActive(true);
        botonFocus.Select();



    }


    public void pausaOff() {

        Time.timeScale = 1;
        panel.SetActive(false);


    }
}

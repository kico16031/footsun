﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPositionGameobjects : MonoBehaviour
{
    public GameObject[] gameObjects;

    private static ResetPositionGameobjects instance;

    public float timeToReset = 1;

    public static ResetPositionGameobjects Instance
    {
        get { return instance; }

    }

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }


    public void ResetGameobjects()
    {
        foreach (GameObject item in gameObjects)
        {
            item.GetComponent<ResetThisGameObject>().resetPosition(timeToReset);
            if (item.GetComponent<CHUTAR>()) {

                item.GetComponent<CHUTAR>().shootReset();


            }
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioCode : int
{
    Music = 0,
    Confirm= 1,
    NesxtOption = 2
}


public class AudioLibrary : MonoBehaviour
{


    public GameObject[] audios;

    public static AudioLibrary instance;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            //DontDestroyOnLoad(this.gameObject);


        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
    
        //audios[(int)AudioCode.Music].GetComponent<AudioSource>().volume = 0.01f;

        audios[(int)AudioCode.Music].GetComponent<AudioSource>().timeSamples = 1;

        audios[(int)AudioCode.Music].GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playSound(int num) {

        audios[num].GetComponent<AudioSource>().Play();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeMain : MonoBehaviour
{


    public Light lighTransition;
    public float velocityLight;

    bool awake = true;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AwakeMainScene());
    }


    private IEnumerator AwakeMainScene()
    {

        yield return new WaitForSeconds(2);

        awake = false;
       

    }

    private void Update()
    {
        if(awake)
        if (lighTransition.range > 0)
            lighTransition.range -= Time.deltaTime * velocityLight;
    }
}

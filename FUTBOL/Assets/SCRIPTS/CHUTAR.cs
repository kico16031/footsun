﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CHUTAR : MonoBehaviour {
    public bool conBalon = false;
	public float Maxfuerza = 500;
	public float MaxTimeShoot = 5;
	public float MaxfuerzaUp = 150;
	public GameObject balon;
	public bool Player2 = false;
	private string miInput;
	Rigidbody rgBalon;
	public float velBalon;
	TerceraPersonaControl control;
	public Light balonLight;
	public ParticleSystem balonPS;
	public float intensityBalonLight;
	public Animator anim;
	public GameObject pierna;
	bool checkPierna = false;
	public AudioSource carga;


	public float forceShoot;

	private void Start()
    {
		rgBalon = balon.GetComponent<Rigidbody>();

		control = GetComponent<TerceraPersonaControl>();
		
	}



    void Update () {


		if (Player2 == false) {
			miInput = "ChutarP1";
		} else {
			miInput = "ChutarP2";
		}

        if (conBalon == true)
        {
			//balon.transform.Rotate(transform.TransformDirection(transform.forward) * 9);

			
			balon.transform.Rotate(Vector3.right * (control.magnitud * velBalon));

			//Quaternion rotation = Quaternion.LookRotation((balon.transform.position - transform.position)*3);
			//rotation *= _facing;
			//balon.transform.rotation = rotation;

			//Vector3 dir = balon.transform.position - transform.position;     // No hace falta normalizar el vector3 ya que no lo vamos a usar para movernos

			//Quaternion quaternion = Quaternion.LookRotation(dir, Vector3.up);

			//balon.transform.rotation = Quaternion.Lerp(balon.transform.rotation, quaternion, 9 * Time.deltaTime);

			//if (forceShoot > 0)
				

			if (Input.GetButtonDown(miInput)) {
				forceShoot = Time.time;
				balonPS.Play();
				anim.SetTrigger("Play");
				checkPierna = true;
				carga.Play();
			}

			if (Input.GetButtonUp(miInput)){
				shoot();


			}

		}


	}

    private void LateUpdate()
    {
		if (checkPierna) { 
			pierna.transform.Rotate(0, 0, 75);
			pierna.transform.GetChild(0).gameObject.transform.Rotate(0, 0, 40);
		}

	}


	public void shoot() {

		carga.Stop();
		forceShoot = Time.time - forceShoot;
		balon.transform.parent = null;
		rgBalon.constraints = RigidbodyConstraints.None;
		if (forceShoot > MaxTimeShoot)
		{
			forceShoot = MaxTimeShoot;
		}
		checkPierna = false;
		rgBalon.AddForce(transform.forward * (Maxfuerza + (forceShoot * 300)));
		//Debug.Log((Maxfuerza + (forceShoot * 50)));
		rgBalon.AddForce(transform.up * MaxfuerzaUp);
		conBalon = false;
		balonPS.Stop();
		anim.SetTrigger("Stop");
		balonLight.intensity = 0;
		forceShoot = 0;
	}

	public void shootReset()
	{
		carga.Stop();
		forceShoot = Time.time - forceShoot;
		balon.transform.parent = null;
		rgBalon.constraints = RigidbodyConstraints.None;
		if (forceShoot > MaxTimeShoot)
		{
			forceShoot = MaxTimeShoot;
		}
		checkPierna = false;
		//rgBalon.AddForce(transform.forward * (Maxfuerza + (forceShoot * 300)));
		//Debug.Log((Maxfuerza + (forceShoot * 50)));
		//rgBalon.AddForce(transform.up * MaxfuerzaUp);
		conBalon = false;
		balonPS.Stop();
		anim.SetTrigger("Stop");
		balonLight.intensity = 0;
		forceShoot = 0;
	}
}

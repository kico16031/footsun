﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeText : MonoBehaviour
{
    public TextMeshProUGUI textEquip1;
    public TextMeshProUGUI textEquip2;


    private void Start()
    {
        if (GameManager1.Instance != null) {
        textEquip1.text = GameManager1.Instance.EQUIPO_1;
        textEquip2.text = GameManager1.Instance.EQUIPO_2;
        }
    }


}

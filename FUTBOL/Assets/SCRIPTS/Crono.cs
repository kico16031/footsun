﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Crono : MonoBehaviour
{
    [Range(0, 99)]
    public int min;
    [Range(0, 60)]
    public float seg;
    public TextMeshProUGUI text;
    public SceneManagerMain scm;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T)) {
            min = 0;
            seg = 5;
            Debug.Log("retorno");
        }

        if (min <= 0 && seg <= 0.1)
        {
            text.text = "00:00";
            scm.SelectedScene("FinPartido",1);


        }
        else
        {
            seg -= Time.deltaTime;
            if (seg <= 0)
            {
                min--;
                seg = 59;
            }
            text.text = System.String.Format("{0:00}", min) + ":" + System.String.Format("{0:00}", seg);
        }
    }
}

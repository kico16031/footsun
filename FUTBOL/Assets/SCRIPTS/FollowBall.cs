﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBall : MonoBehaviour
{
    public GameObject ball;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ball.transform.position.x > -3.35 && ball.transform.position.x < 11.2) {
        //transform.position = new Vector3(ball.transform.position.x, transform.position.y, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, new Vector3(ball.transform.position.x, transform.position.y, transform.position.z), Time.deltaTime * 3);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager1 : MonoBehaviour
{
    private static GameManager1 instance;

    public string EQUIPO_1 = "Equipo 1";
    public string EQUIPO_2 = "Equipo 2";
    public int PUNT_EQUIPO_1 = 0;
    public int PUNT_EQUIPO_2 = 0;

    public static GameManager1 Instance {
        get { return instance; }
    
    }

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

  

}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gol : MonoBehaviour
{

    public Animator anim;

    public TextMeshProUGUI text;

    bool count = true;

    public AudioSource audioGol;

    [Header("ACTIVADO: EQUIPO 1 - DESACTIVADO: EQUIPO2")]
    public bool equipo = true;



    private void OnTriggerEnter(Collider other)
    {

        if (count)
            if (other.tag == "Pelota")
            {
                audioGol.Play();
                count = false;
                anim.SetTrigger("Play");
                text.text = (int.Parse(text.text) + 1).ToString();

                ResetPositionGameobjects.Instance.ResetGameobjects();
                if(GameManager1.Instance!=null)
                if (equipo)
                    GameManager1.Instance.PUNT_EQUIPO_1 = int.Parse(text.text);
                else
                    GameManager1.Instance.PUNT_EQUIPO_2 = int.Parse(text.text);
            }
        StartCoroutine("noCount");


    }


    IEnumerator noCount()
    {
        yield return new WaitForSeconds(1);
        count = true;


    }
}

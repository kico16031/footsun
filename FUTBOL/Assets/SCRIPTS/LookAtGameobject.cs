﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtGameobject : MonoBehaviour
{

    public GameObject gameobject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(gameobject.transform.position);
    }
}

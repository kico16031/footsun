﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextScene : MonoBehaviour
{
    SceneManagerMain sm;

    public string nameScene;
    // Start is called before the first frame update
    void Start()
    {
        sm = gameObject.GetComponent<SceneManagerMain>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            sm.SelectedScene(nameScene);
        }

        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    sm.exitTo();
        //}
    }
}

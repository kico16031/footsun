﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetThisGameObject : MonoBehaviour
{

    //public bool reset = false;
    float timeToReset = 1;
    Vector3 startPosition;
    Quaternion startRotation;

    CharacterController controller;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;

        controller = gameObject.GetComponent<CharacterController>();

        rb = gameObject.GetComponent<Rigidbody>();


    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (reset)
    //    {
    //        gameObject.GetComponent<CharacterController>().enabled = false;
    //        StartCoroutine("resetPos");
    //    }

    //    if (Input.GetKeyDown(KeyCode.E))
    //    {
    //        resetPosition();


    //    }
    //}

    public void resetPosition(float time)
    {
        if (controller) {
            controller.enabled = false;
        }
        timeToReset = time;
        StartCoroutine("resetPos");


    }

    IEnumerator resetPos()
    {
        if (rb)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
        yield return new WaitForSeconds(timeToReset);
        transform.position = startPosition;
        transform.rotation = startRotation;
        if (rb) {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }

        if (controller)
        {
            controller.enabled = true;
        }
    }
}

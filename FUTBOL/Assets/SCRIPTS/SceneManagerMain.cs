﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerMain : MonoBehaviour
{
    // Start is called before the first frame update

    public Light lighTransition;
    public float velocityLight;

    bool tomain = false;


    public void exitTo()
    {

        StartCoroutine(toExit());
    }

    private IEnumerator toExit()
    {

        yield return new WaitForSeconds(1);
        Application.Quit();

    }

    public void MainScene()
    {
        tomain = true;
        StartCoroutine(toMainScene());
    }

    private IEnumerator toMainScene()
    {
        

         yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Main");

    }

    public void SelectedScene(string nameScene)
    {
        tomain = true;
        StartCoroutine(toSelectedScene(nameScene));
    }

    public void SelectedScene(string nameScene, int time)
    {
        tomain = true;
        StartCoroutine(toSelectedScene(nameScene, time));
    }

    private IEnumerator toSelectedScene(string nameScene)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(nameScene);

    }

    private IEnumerator toSelectedScene(string nameScene,int time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(nameScene);

    }

    public void GameScene()
    {
        tomain = true;
        StartCoroutine(toGameScene());
    }

    private IEnumerator toGameScene()
    {
       

        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Scene_FUTBOL");

    }

    public void IntroGameScene()
    {
        tomain = true;
        StartCoroutine(toIntroGameScene());
    }

    private IEnumerator toIntroGameScene()
    {
        

        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("IntroPartido");

    }

    private void Update()
    {
        if (tomain) {
            if (lighTransition.range < 100)
                lighTransition.range += Time.deltaTime * velocityLight;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectEquipo : MonoBehaviour
{
    [Header("ACTIVADO: EQUIPO 1 - DESACTIVADO: EQUIPO2")]
    public bool equipo = true;

    [Header("NOMBRE EQUIPO")]
    public string equipoName = "Equipo";

    public void Selected()
    {
        if (equipo)
            GameManager1.Instance.EQUIPO_1 = equipoName;
        else
            GameManager1.Instance.EQUIPO_2 = equipoName;
    }
}

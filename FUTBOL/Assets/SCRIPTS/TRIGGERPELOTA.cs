﻿using UnityEngine;
using System.Collections;

public class TRIGGERPELOTA : MonoBehaviour {
	public CHUTAR chutarscript;
	Rigidbody PelotaRB;


	// Use this for initialization
	void Start () {
		PelotaRB = GameObject.FindGameObjectWithTag("Pelota").GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == "Pelota") {
			
			PelotaRB.constraints = RigidbodyConstraints.FreezeAll;

			PelotaRB.transform.parent = transform;
			PelotaRB.transform.localPosition = new Vector3 (0, 0, 0);
			other.gameObject.transform.rotation = transform.parent.rotation;
            chutarscript.conBalon = true;
		}

	}
	void OnTriggerExit(Collider other)
	{
		if (other.transform.tag == "Pelota") {

			chutarscript.conBalon = false;


		}

	}

}

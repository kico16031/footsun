﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
public class TerceraPersonaControl : MonoBehaviour {
	Transform thisTransform ;
	public float speed = 6.0F;
	public float rotateSpeed = 10.0F;
	public float gravity = 20.0F;
	public float magnitud;

	public Transform ref_piv_cam;

	private Vector3 moveDirection = Vector3.zero;

	public bool SOYPLAYERUNO = true;
	CharacterController controller; 

	public Animator anim;
	//collider ataques


	void Start(){
		thisTransform = GetComponent<Transform>();
		controller = GetComponent<CharacterController>();
	}
	void Update(){



			if (controller.isGrounded) {

				//Relativo a Camara
			if (SOYPLAYERUNO == true) {
				moveDirection = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical")));
			} else {
				moveDirection = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Mouse X"), 0, Input.GetAxis ("Mouse Y")));
			}

			moveDirection = moveDirection.normalized;
				moveDirection *= speed;
		
			} 



			//ROTACION
		if (SOYPLAYERUNO == true) {
			Vector3 horizontalVelocity = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical")));
		

			magnitud = horizontalVelocity.magnitude;
			horizontalVelocity.y = 0;
			if (horizontalVelocity.magnitude > 0.1f) {

				thisTransform.forward = horizontalVelocity.normalized;

			}
			anim.SetFloat ("magnitud", magnitud); 

		} else {
			Vector3 horizontalVelocity = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Mouse X"), 0, Input.GetAxis ("Mouse Y")));


			magnitud = horizontalVelocity.magnitude;
			horizontalVelocity.y = 0;
			if (horizontalVelocity.magnitude > 0.1f) {

				thisTransform.forward = horizontalVelocity.normalized;

			}

			anim.SetFloat ("magnitud", magnitud); 

		}
		//APLICA MOVIMIENTO
	

		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move (moveDirection * Time.deltaTime);


		


	}

		
}







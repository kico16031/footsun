﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
public class TerceraPersonaControlAlt : MonoBehaviour {
	Transform thisTransform ;
	public float speed = 6.0F;
	public float rotateSpeed = 10.0F;
	public float gravity = 20.0F;
	//CharacterController controller;
	public Transform ref_piv_cam;

	private Vector3 moveDirection = Vector3.zero;

	public bool SOYPLAYERUNO = true;
		

	public Animator anim;
	//collider ataques

	Rigidbody rg;

	void Start(){
		thisTransform = GetComponent<Transform>();
		rg = gameObject.GetComponent<Rigidbody>();
		moveDirection = transform.position;
		//controller = GetComponent<CharacterController>();

	}
	void Update(){

			

			//if (controller.isGrounded) {

				//Relativo a Camara
			if (SOYPLAYERUNO == true) {
				moveDirection = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical")));
			} else {
				moveDirection = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Mouse X"), 0, Input.GetAxis ("Mouse Y")));
			}


				
		



				
			//} 



			//ROTACION
		if (SOYPLAYERUNO == true) {
			Vector3 horizontalVelocity = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical")));
			

			float magnitud = horizontalVelocity.magnitude;
			horizontalVelocity.y = 0;
			if (horizontalVelocity.magnitude > 0.1f) {

				thisTransform.forward = horizontalVelocity.normalized;

			}
			anim.SetFloat ("magnitud", magnitud); 

		} else {
			
			
			Vector3 horizontalVelocity = ref_piv_cam.TransformDirection (new Vector3 (Input.GetAxis ("Mouse X"), 0, Input.GetAxis ("Mouse Y")));


			float magnitud = horizontalVelocity.magnitude;
			horizontalVelocity.y = 0;
			if (horizontalVelocity.magnitude > 0.1f) {

				thisTransform.forward = horizontalVelocity.normalized;

			}

			anim.SetFloat ("magnitud", magnitud); 

		}
		//APLICA MOVIMIENTO


		//moveDirection.y -= gravity * Time.deltaTime;
		//controller.Move (moveDirection * Time.deltaTime);

		//transform.Translate(moveDirection * Time.deltaTime);
		//Vector3.MoveTowards(transform.position, moveDirection, Time.deltaTime * speed);

		moveDirection = moveDirection.normalized;
		//moveDirection *= speed;
		
		
		//rg.MovePosition(moveDirection);


		//if (moveDirection != Vector3.zero)

		//gameObject.GetComponent<Rigidbody>().MovePosition(moveDirection);


	}

    private void LateUpdate()
    {
		moveDirection = transform.position + moveDirection * (speed * Time.fixedDeltaTime);
		rg.MovePosition(moveDirection);
		
		//rg.MovePosition(moveDirection);
    }

}







﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AcceptList : MonoBehaviour
{
    public Button[] buttons;
    public Button nextFirtsButton;
    public Button backFirtsButton;

    


    public void interectableButtons(bool inter)
    {

        foreach (Button item in buttons)
        {
            item.interactable = inter;


        }

       

        if (inter)
        {
            if (!nextFirtsButton.IsInteractable())
                nextFirtsButton.interactable = true;

            nextFirtsButton.Select();
            PanelExibicionActivo.instance.idRef++;
        }
        else
        {
            backFirtsButton.Select();
            PanelExibicionActivo.instance.idRef--;

        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActivateArrows : MonoBehaviour, ISelectHandler, IDeselectHandler
{

    Button thisButton;
    public GameObject arrows;
    // Start is called before the first frame update
    void Start()
    {
        thisButton = gameObject.GetComponent<Button>();
    }


    public void OnSelect(BaseEventData eventData)
    {
        arrows.SetActive(true);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        arrows.SetActive(false);
    }

}

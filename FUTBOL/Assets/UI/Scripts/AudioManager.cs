﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public float intensity;
    public float velocityRotate;
    public float minRotate;
    public float alphaTitle;
    public float minalphaTitle;
    public float despMove;
    public float velocityPent;

    public bool wait;
    AudioSource audio;
    //public static float[] sample = new float[512];

    //public float valurSound;
    public Light lighVolum;
    public Light lighRefl;

    public GameObject pentagonos;

    //public Transform PentCampeona;
    //public Transform PentExhibicion;
    //public Transform PentSalir;
    //public Transform PentOpciones;
    //public Transform PentAbajo;
    //public Transform PentPanel;


    public SpriteRenderer srTitle;


    //// Start is called before the first frame update
    //void Start()
    //{
    //    audio = GetComponent<AudioSource>();
    //}

    //private void Update()
    //{
    //    GetSpectrumAudioSource();

    //    for (int i = 0; i < sample.Length; i++)
    //    {
    //        valurSound += sample[i];
    //    }

    //    valurSound = valurSound / sample.Length;

    //    valurSound = valurSound *100;

    //    lighy.intensity = valurSound;
    //}

    //void GetSpectrumAudioSource() {

    //    audio.GetSpectrumData(sample, 0, FFTWindow.Blackman);
    //}

    int qSamples  = 1024;  // array size
    float refValue = 0.1f; // RMS value for 0 dB
 float threshold = 0.02f;      // minimum amplitude to extract pitch
    float rmsValue;   // sound level - RMS
    float dbValue;    // sound level - dB
 float pitchValue; // sound pitch - Hz
 
 private float[] samples; // audio samples
 private float[] spectrum; // audio spectrum
 private float fSample;
 
 void Start()
    {
        audio = GetComponent<AudioSource>();
        samples = new float[qSamples];
        spectrum = new float[qSamples];
        fSample = AudioSettings.outputSampleRate;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void AnalyzeSound()
    {
        audio.GetOutputData(samples, 0); // fill array with samples
        int i;
        float sum = 0;
        for (i = 0; i < qSamples; i++)
        {
            sum += samples[i] * samples[i]; // sum squared samples
        }
        rmsValue = Mathf.Sqrt(sum / qSamples); // rms = square root of average
        dbValue = 20 * Mathf.Log10(rmsValue / refValue); // calculate dB
        if (dbValue < -160) dbValue = -160; // clamp it to -160dB min
                                            // get sound spectrum
        audio.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        float maxV= 0;
        int maxN = 0;
        for (i = 0; i < qSamples; i++)
        { // find max 
            if (spectrum[i] > maxV && spectrum[i] > threshold)
            {
                maxV = spectrum[i];
                maxN = i; // maxN is the index of max
            }
        }
        float freqN= maxN; // pass the index to a float variable
        if (maxN > 0 && maxN < qSamples - 1)
        { // interpolate index using neighbours
            var dL = spectrum[maxN - 1] / spectrum[maxN];
            var dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        pitchValue = freqN * (fSample / 2) / qSamples; // convert index to frequency
    }

    public string display; // drag a GUIText here to show results

 
void Update()
    {
        //if (Input.GetKeyDown("p"))
        //{
        //    audio.Play();
        //}
        AnalyzeSound();
        //if (display)
        //{
            display = "RMS: " + rmsValue.ToString("F2") +
            " (" + dbValue.ToString("F1") + " dB)\n" +
            "Pitch: " + pitchValue.ToString("F0") + " Hz";
        //}

        lighVolum.intensity = rmsValue * 1000000000 * intensity;
        lighRefl.intensity = rmsValue * 1000000000000 * intensity;

        if (rmsValue * alphaTitle > minalphaTitle)
        {

            srTitle.color = new Color(srTitle.color.r, srTitle.color.g, srTitle.color.b, rmsValue * alphaTitle);

        }
        else {

            srTitle.color = new Color(srTitle.color.r, srTitle.color.g, srTitle.color.b, minalphaTitle);
        }

        if ((rmsValue * velocityRotate) < minRotate) {
            RotateSingleton.instance.velocity = minRotate;
            if (!wait) {
                wait = true;
                RotateSingleton.instance.inverseDirection = !(RotateSingleton.instance.inverseDirection);
                StartCoroutine(toWait());

            }
        }
        else
        RotateSingleton.instance.velocity = rmsValue * velocityRotate;

        //PentCampeona.transform.position = new Vector3(PentCampeona.transform.position.x, PentCampeona.transform.position.y, (PentPanel.transform.position.z + rmsValue * despMove));//Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, (-10 + rmsValue * despMove)) , velocityPent * Time.deltaTime); 
        //PentExhibicion.transform.position = new Vector3((PentCampeona.transform.position.x + rmsValue * despMove), PentExhibicion.transform.position.y, PentExhibicion.transform.position.z);//  Vector3.Lerp(transform.position, new Vector3((10 + rmsValue * despMove), transform.position.y, transform.position.z), velocityPent * Time.deltaTime); ;
        //PentSalir.transform.position = new Vector3((PentCampeona.transform.position.x + rmsValue * despMove), PentSalir.transform.position.y, PentSalir.transform.position.z);// Vector3.Lerp(transform.position, new Vector3((-10 + rmsValue * despMove), transform.position.y, transform.position.z), velocityPent * Time.deltaTime); ;
        //PentOpciones.transform.position = new Vector3(PentOpciones.transform.position.x, PentOpciones.transform.position.y, (PentPanel.transform.position.z + rmsValue * despMove));// Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, (10 + rmsValue * despMove)), velocityPent * Time.deltaTime); ;
        //PentAbajo.transform.position = new Vector3(PentAbajo.transform.position.x, (PentCampeona.transform.position.y + rmsValue * despMove), PentAbajo.transform.position.z);// Vector3.Lerp(transform.position, new Vector3(transform.position.x, (-10 + rmsValue * despMove), transform.position.z), velocityPent * Time.deltaTime); ;
        //PentPanel.transform.position = new Vector3(PentPanel.transform.position.x, (PentCampeona.transform.position.y + rmsValue * despMove), PentPanel.transform.position.z); // Vector3.Lerp(transform.position, new Vector3(transform.position.x, (10 + rmsValue * despMove), transform.position.z), velocityPent * Time.deltaTime); ;

        pentagonos.transform.localScale = Vector3.Lerp(pentagonos.transform.localScale, new Vector3(0.9f + (rmsValue * despMove), 0.9f + (rmsValue * despMove), 0.9f + (rmsValue * despMove)), velocityPent * Time.deltaTime); 


    }


    private IEnumerator toWait()
    {

        yield return new WaitForSeconds(2);
        wait = false;

    }


}

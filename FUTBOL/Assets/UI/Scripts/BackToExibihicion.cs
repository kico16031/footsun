﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackToExibihicion : MonoBehaviour
{
    public Button[] buttons;
    public Button nextFirtsButton;


    public void back() {

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = true;
           

        }
        nextFirtsButton.Select();
        PanelExibicionActivo.instance.idRef--;
        gameObject.GetComponent<Button>().interactable = false;

    }

}

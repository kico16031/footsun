﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerUI : MonoBehaviour
{
    public GameObject[] interfaces;
    public Button[] firtsButton;

    public int position;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            BackInterface();

        }
    }


    public void NextInterface()
    {
        if (position + 1 <= interfaces.Length)
        {
            position++;
            ClearInterfaces();
        }
        
    }

    public void BackInterface()
    {
        if (position - 1 >= 0)
        {
            position--;
            ClearInterfaces();
        }

    }

    private void ClearInterfaces()
    {

        for (int i = 0; i < interfaces.Length; i++)
        {
            if (i == position)
            {
                interfaces[i].SetActive(true);
                firtsButton[i].Select();

            }
            else
            {
                interfaces[i].SetActive(false);

            }


        }

        if (position == 0) { 
            SunController.instance.disabledRotate(false);
            MoveCamera.instance.GoToPosition(0);
        } else { 
            SunController.instance.disabledRotate(true); 
        }
    }


}

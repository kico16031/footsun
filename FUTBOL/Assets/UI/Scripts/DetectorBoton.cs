﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectorBoton : MonoBehaviour
{
    RaycastHit hit;

    public int distanceRay = 100;

    GameObject boton;

    int layer; //= LayerMask.NameToLayer("Botones");
    // Start is called before the first frame update
    void Start()
    {
        layer = LayerMask.NameToLayer("Botones");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        //Physics.

        if (Physics.Raycast(transform.position, fwd, out hit, distanceRay  ))
        {

            //if(hit.collider)
            //hit.collider.gameObject.GetComponent<Button>().Select();
            //Debug.Log("GAMEOBJECT:  " + hit.collider.gameObject.name);
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Botones"))
            {
                if(boton!= hit.collider.gameObject)
                    hit.collider.gameObject.GetComponent<Button>().Select();

                boton = hit.collider.gameObject;
                

            }


        }

       // Debug.Log("eeeeeeeeeeeeeeee:  " + layer);
    }

    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
         Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, fwd * distanceRay);
    }
}

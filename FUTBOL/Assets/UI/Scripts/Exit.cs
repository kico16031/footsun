﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    // Start is called before the first frame update
   

    public void exitTo() {

        StartCoroutine(toExit());
    }

    private IEnumerator toExit()
    {

            yield return new WaitForSeconds(1);
        Application.Quit();

    }
}

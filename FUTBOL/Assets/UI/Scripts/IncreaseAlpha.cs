﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IncreaseAlpha : MonoBehaviour
{
    TextMeshProUGUI thistext;
    bool active;
    float velocityIncrease = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
        thistext = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        thistext.color = new Color(thistext.color.r, thistext.color.g, thistext.color.b, 0);


    }

    // Update is called once per frame
    void Update()
    {
        if (active) {
            if (thistext.color.a < 1)
            {
                thistext.color = new Color(thistext.color.r, thistext.color.g, thistext.color.b, thistext.color.a + (velocityIncrease * Time.deltaTime));

            }
            else
                active = false;
        }
    }

    void OnDisable()
    {
        thistext.color = new Color(thistext.color.r, thistext.color.g, thistext.color.b, 0);
        active = false;
    }

    void OnEnable()
    {
        active = true;
    }
}

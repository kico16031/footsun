﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Vector3[] posiciones;
    public Vector3[] rotaciones;

    public Vector3 posicion;
    public Vector3 rotacion;

    public int velocidad;

    public static MoveCamera instance;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            //DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
     
        posicion = posiciones[0];
        rotacion = rotaciones[0];
    }

    // Update is called once per frame
    void Update()
    {
      

        RotateCamera(rotacion);
        TranslateCamera(posicion);
    }

    public void GoToPosition(int pos) {
        posicion = posiciones[pos];
        rotacion = rotaciones[pos];



    }

    void TranslateCamera(Vector3 posicion)
    {


        transform.position = Vector3.Lerp(transform.position, posicion, velocidad * Time.deltaTime); 




    }
    void RotateCamera(Vector3 posicion)
    {


        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(posicion), velocidad * Time.deltaTime);
       

       
    }
}

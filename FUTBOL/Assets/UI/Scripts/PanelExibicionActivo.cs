﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelExibicionActivo : MonoBehaviour
{
    public bool panelActive = false;
    public Button firstButton;
    public Button backButton;
    public Button buttonSelect;
    public AcceptList listaD;
    public BackToExibihicion buttonAcept;
    public int idRef;

    public static PanelExibicionActivo instance;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }



    // Update is called once per frame
    void Update()
    {
        if (panelActive)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                switch (idRef)
                {
                    case 0:
                        SetActivePanel(false);
                        break;
                    case 1:
                        if (buttonSelect != null) { buttonSelect.interactable = true; }
                        listaD.interectableButtons(false);
                        break;
                    case 2:
                        buttonAcept.back();
                        break;
                    default:
                        break;
                }

            }
        }

    }

    public void SetActivePanel(bool active)
    {
        panelActive = active;
        if (active)
        {
            firstButton.Select();
            SunController.instance.disabledRotate(true);
            SunController.instance.SetPositionSun(0);





        }
        else
        {
            backButton.Select();
            SunController.instance.disabledRotate(false);
            SunController.instance.SetPositionSun(1);
            MoveCamera.instance.GoToPosition(0);
        }


    }
}

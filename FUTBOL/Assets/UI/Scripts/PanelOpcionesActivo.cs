﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelOpcionesActivo : MonoBehaviour
{
    public bool panelActive = false;
    public GameObject canvas;
    public Button firstButton;
    public Button backButton;
    //public int idRef;

    //public static PanelOpcionesActivo instance;

    //void Awake()
    //{

    //    if (instance == null)
    //    {

    //        instance = this;
    //        DontDestroyOnLoad(this.gameObject);

    //        //Rest of your Awake code

    //    }
    //    else
    //    {
    //        Destroy(this);
    //    }
    //}



    // Update is called once per frame
    void Update()
    {
        if (panelActive)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SetActivePanel(false);

            }
        }

    }

    public void SetActivePanel(bool active)
    {
        panelActive = active;
        if (active)
        {
            SunController.instance.disabledRotate(true);
            canvas.SetActive(true);
            firstButton.Select();
            //SunController.instance.SetPositionSun(0);





        }
        else
        {
            backButton.Select();
            SunController.instance.disabledRotate(false);
            SunController.instance.SetPositionSun(2);
            MoveCamera.instance.GoToPosition(0);
            canvas.SetActive(false);
        }


    }
}

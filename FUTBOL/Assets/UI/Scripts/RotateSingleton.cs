﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSingleton : MonoBehaviour
{
    public float velocity;
    public bool inverseDirection;

    public static RotateSingleton instance;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(inverseDirection)
            transform.Rotate(Vector3.up * velocity/100);
        else
            transform.Rotate(Vector3.down * velocity / 100);
    }
}

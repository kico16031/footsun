﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectButtonEx : MonoBehaviour
{
    Button thisButton;

    // Start is called before the first frame update
    void Start()
    {
        thisButton = this.gameObject.GetComponent<Button>();
    }

    public void interectableButton() {

        thisButton.interactable = false;


    }

    public void selectButton()
    {

        PanelExibicionActivo.instance.buttonSelect = thisButton;


    }

   
}

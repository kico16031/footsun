﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class SunController : MonoBehaviour
{
    public Vector3[] posiciones;
    public Vector3 posicionActual;
    public int velocidad;

    private bool disableRotate = false;

    private int indexPosiciones = 0;

    public static SunController instance;

    void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!disableRotate)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                AudioLibrary.instance.playSound((int)AudioCode.NesxtOption);
                indexPosiciones++;
                if (indexPosiciones > posiciones.Length - 1)
                {
                    indexPosiciones = 0;
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                AudioLibrary.instance.playSound((int)AudioCode.NesxtOption);
                indexPosiciones--;
                if (indexPosiciones < 0)
                {
                    indexPosiciones = posiciones.Length - 1;

                }
            }

           
        }

        rotateSun(posiciones[indexPosiciones]);
    }

    public void SetPositionSun(int numpos) {
        indexPosiciones = numpos;
    }

    void rotateSun(Vector3 posicion)
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(posicion), velocidad * Time.deltaTime);

        posicionActual = posicion;

        
    }

    public void disabledRotate(bool boolean)
    {
        disableRotate = boolean;
    }
}

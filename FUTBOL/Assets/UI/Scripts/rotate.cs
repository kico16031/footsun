﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    public float velocity;
    public bool inverseDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(inverseDirection)
            transform.Rotate(Vector3.up * velocity/100);
        else
            transform.Rotate(Vector3.down * velocity / 100);
    }
}

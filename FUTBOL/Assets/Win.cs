﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    [Header("Gana equipo 1")]
    public GameObject[] win1;

    [Header("Gana equipo 2")]
    public GameObject[] win2;

    [Header("Empate")]
    public GameObject[] empate;

    // Start is called before the first frame update
    void Start()
    {
        if (GameManager1.Instance.PUNT_EQUIPO_1 == GameManager1.Instance.PUNT_EQUIPO_2)
        {
            foreach (GameObject item in empate)
            {
                item.SetActive(true);
            }
        }
        else if (GameManager1.Instance.PUNT_EQUIPO_1 > GameManager1.Instance.PUNT_EQUIPO_2)
        {
            foreach (GameObject item in win1)
            {
                item.SetActive(true);
            }
        }
        else
        {

            foreach (GameObject item in win2)
            {
                item.SetActive(true);
            }
        }

        GameManager1.Instance.EQUIPO_1 = "Equipo 1";
        GameManager1.Instance.EQUIPO_2 = "Equipo 2";
        GameManager1.Instance.PUNT_EQUIPO_1 = 0;
        GameManager1.Instance.PUNT_EQUIPO_2 = 0;
    }
}
